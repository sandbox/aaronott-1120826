#!/bin/bash

# Download Drush v4 from D.O and make it work on `drush` (OS X / Linux / *nix)
# Written by stemount, adapted by KarenS, then adapted by aaronott
# last update by aaronott April 07, 2011
# First test environment if there is no php there is no reason to move on
php=`which php 2>&1`
ret=$?
if [ $ret -ne 0 ] || ! [ -x $php ]; then
  echo "drush cannot be installed without php." >&2
  echo "Install php first, and then try again." >&2
  exit $ret
fi

# move to home dir
cd ~

# drush might already be installed. Let's check
drush=`which drush 2>&1`
ret=$?
if [ $ret -ne 1 ] || [ -f ".drush/drush/drush" ]; then
    echo "It would appear as though drush is already installed." >&2
    echo "If you would like to upgrade it please run: drush dl drush" >&2
    exit $ret
fi

# remove current drush (if existent)
rm -rf ~/.drush/
# create drush directory (and hide it)
mkdir ~/.drush/
# move to new dir
cd ~/.drush/

# Next check to see if git exists
git=`which git 2>&1`
ret=$?
if [ $ret -ne 0 ] || ! [ -x $git ]; then
    # This is fine, we will just download the latest 4.x dev
    drush_dl = "drush-All-versions-4.x-dev.tar.gz"
    echo "No git found, we'll just download the latest package." >&2
    url=`curl -C - -O -s http://ftp.drupal.org/files/projects/$drush_dl`
    echo "fetching: $url" >&2
  
    ret=$?
    if [ $ret -ne 0 ]; then
      echo "Failed to get tarball url" >&2
      exit $ret
    fi
  
    tar -zxf $drush_dl
    rm $drush_dl
  else
    echo "Using git to download." >&2
    $git clone http://git.drupal.org/project/drush.git >&2
    cd drush
    # get the latest tag from the 7.x branch
    version=`git tag | grep 7.x | tail -1`
    echo "Checking out $version." >&2
    $git checkout -q $version
fi

# make drush command executable
cd ~/.drush/
if [ ! -x drush/drush ]; then
  chmod u+x drush/drush
fi

# make sure that we alias drush just in case source does not work
drush='~/.drush/drush/drush'

# alias drush and put in bash profile
echo "alias drush='$drush'" >> ~/.bash_profile
source ~/.bash_profile

ret=$?
if [ $ret -ne 0 ]; then
  echo "It failed" >&2
else
  echo "Drush should now be installed. type 'drush' for commands."
fi

exit $ret
