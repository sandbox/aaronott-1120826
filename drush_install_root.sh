#!/bin/bash

# Download Drush v4 from D.O and make it work on `drush` (OS X / Linux / *nix)
# Based on that script, this one allows for a system wide install of drush
# Written by stemount, adapted by KarenS, then adapted by aaronott
# last update by aaronott April 07, 2011

DRUSH_DIR='/usr/share';
LINK_DIR='/usr/local/bin';
# First test environment if there is no php there is no reason to move on
php=`which php 2>&1`
ret=$?
if [ $ret -ne 0 ] || ! [ -x $php ]; then
  echo "drush cannot be installed without php." >&2
  echo "Install php first, and then try again." >&2
  exit $ret
fi

# drush might already be installed. Let's check
drush=`which drush 2>&1`
ret=$?
if [ $ret -ne 1 ]; then
    echo "It would appear as though drush is already installed in $drush" >&2
    echo "If you would like to upgrade it please run: drush dl drush" >&2
    exit $ret
fi

# check to see if we can even write to the dir
if ! [ -w $DRUSH_DIR ] || ! [ -w $LINK_DIR ]; then
  echo "You will need to have permissions to write to $DRUSH_DIR" >&2
  echo "as well as create a link in $LINK_DIR" >&2
  exit
fi

cd $DRUSH_DIR

# Next check to see if git exists
git=`which git 2>&1`
ret=$?
if [ $ret -ne 0 ] || ! [ -x $git ]; then
    # This is fine, we will just download the latest 4.x dev
    drush_dl = "drush-All-versions-4.x-dev.tar.gz"

    echo "No git found, we'll just download the latest package." >&2
    url=`curl -C - -O -s http://ftp.drupal.org/files/projects/$drush_dl`
    echo "fetching: $url" >&2
  
    ret=$?
    if [ $ret -ne 0 ]; then
      echo "Failed to get tarball url" >&2
      exit $ret
    fi
  
    tar -zxf $drush_dl
    rm $drush_dl

    cd drush
  else
    echo "Using git to download." >&2
    $git clone http://git.drupal.org/project/drush.git >&2
    cd drush
    # get the latest tag from the 7.x branch
    version=`git tag | grep 7.x | tail -1`
    echo "Checking out $version." >&2
    $git checkout -q $version
fi

if [ ! -x ./drush ]; then
  chmod u+x ./drush
fi

# create a link
ln -s $DRUSH_DIR/drush/drush $LINK_DIR/drush

# since we are here and we know we have permission, we need to run
# drush for the first time so it can download the PEAR Console_Table stuff
version=`drush --version`
ret=$?
if [ $ret -ne 0 ]; then
  echo "It failed" >&2
else
  echo "Drush should now be installed. type 'drush' for commands."
fi

exit $ret
